#include "header.h"

void	routine_philo(t_rule *rule)
{
	int	i;
	int	time;
	int	last_eat;

	i = -1;
	time = get_time_ms(rule);
	while (++i < rule->nb)
	{
		pthread_mutex_lock(rule->philo[i].m_meal);
		last_eat = rule->philo[i].last_eat;
		pthread_mutex_unlock(rule->philo[i].m_meal);
		if (last_eat + rule->t_die <= time)
		{
			if (rule->philo[i].end == 1)
				return ;
			pthread_mutex_lock(&rule->m_lock);
			if (rule->lock == 0)
				rule->lock = 1;
			printf("%dms\t%d %s\n", get_time_ms(rule), i + 1, DIE);
			pthread_mutex_unlock(&rule->m_lock);
			return ;
		}
	}
}

void	even_one_day_of_life(t_rule *rule, t_philo *philo, int *j)
{
	pthread_mutex_lock(philo->m_l_fork);
	write_action(philo, rule, LFORK);
	pthread_mutex_lock(philo->m_r_fork);
	write_action(philo, rule, RFORK);
	pthread_mutex_lock(philo->m_meal);
	philo->last_eat = get_time_ms(rule);
	write_action(philo, rule, EAT);
	philo->cycles++;
	if (philo->cycles == rule->num_eat)
		philo->end = 1;
	pthread_mutex_unlock(philo->m_meal);
	standing_action(rule, rule->t_eat);
	pthread_mutex_unlock(philo->m_r_fork);
	pthread_mutex_unlock(philo->m_l_fork);
	write_action(philo, rule, SLEEP);
	standing_action(rule, rule->t_sleep);
	write_action(philo, rule, THINK);
	if (rule->t_eat >= rule->t_sleep)
		standing_action(rule, (rule->t_eat - rule->t_sleep) + 1);
	pthread_mutex_lock(&rule->m_lock);
	*j = rule->lock;
	pthread_mutex_unlock(&rule->m_lock);
}

void	odd_one_day_of_life(t_rule *rule, t_philo *philo, int *j)
{
	pthread_mutex_lock(philo->m_r_fork);
	write_action(philo, rule, RFORK);
	pthread_mutex_lock(philo->m_l_fork);
	write_action(philo, rule, LFORK);
	pthread_mutex_lock(philo->m_meal);
	philo->last_eat = get_time_ms(rule);
	write_action(philo, rule, EAT);
	philo->cycles++;
	if (philo->cycles == rule->num_eat)
		philo->end = 1;
	pthread_mutex_unlock(philo->m_meal);
	standing_action(rule, rule->t_eat);
	pthread_mutex_unlock(philo->m_l_fork);
	pthread_mutex_unlock(philo->m_r_fork);
	write_action(philo, rule, SLEEP);
	standing_action(rule, rule->t_sleep);
	write_action(philo, rule, THINK);
	if (rule->t_eat >= rule->t_sleep)
		standing_action(rule, (rule->t_eat - rule->t_sleep) + 1);
	pthread_mutex_lock(&rule->m_lock);
	*j = rule->lock;
	pthread_mutex_unlock(&rule->m_lock);
}

void	standing_action(t_rule *rule, int time_ms)
{
	int	start;
	int	current;

	start = get_time_ms(rule);
	current = start;
	while (current < start + time_ms)
	{
		usleep(42);
		current = get_time_ms(rule);
	}
}

void	*multi_philos_cycle(void *arg)
{
	t_philo	*philo;
	t_rule	*rule;
	int		j;

	philo = (t_philo *)arg;
	rule = (t_rule *)philo->ptr;
	j = 0;
	philo->cycles = 0;
	while ((philo->cycles < rule->num_eat || !rule->max_eat) && !j)
	{
		if (philo->id % 2 == 1)
			odd_one_day_of_life(rule, philo, &j);
		else
			even_one_day_of_life(rule, philo, &j);
	}
	pthread_mutex_lock(&rule->m_lock);
	if (rule->lock == 0)
		rule->lock = 2;
	pthread_mutex_unlock(&rule->m_lock);
	return (NULL);
}
