#include "header.h"

static int	ft_isdigit(const int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

size_t	ft_strlen(const char *s)
{
	size_t	i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

void	quit_convert_arg(char *err, char *txt)
{
	printf("%s %s\n", err, txt);
	exit(EXIT_FAILURE);
}

int	convert_arg(const char *str)
{
	long	nb;
	int		i;

	i = 0;
	nb = 0;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			quit_convert_arg(ERR_I, "value can't be negative");
		i++;
	}
	while (ft_isdigit(str[i]))
	{
		nb = nb * 10 + str[i] - 48;
		i++;
		if (nb > INT_MAX || (ft_strlen(str) > ft_strlen("2147483647")))
			quit_convert_arg(ERR_I, "value too hight");
	}
	if (!ft_isdigit(str[i]) && str[i] != '\0')
		quit_convert_arg(ERR_I, "invalid character");
	return (nb);
}
