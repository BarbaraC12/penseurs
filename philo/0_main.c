#include "header.h"

static void	join_and_destroy_mutex(t_rule *rule, pthread_t *tid)
{
	int	i;

	i = 0;
	while (i < rule->nb)
		pthread_join(tid[i++], NULL);
	pthread_mutex_destroy(&rule->m_lock);
	i = 0;
	while (i < rule->nb)
	{
		pthread_mutex_destroy(&rule->m_fork[i]);
		i++;
	}
}

void	write_action(t_philo *philo, t_rule *rule, char *str)
{
	int	i;

	pthread_mutex_lock(&rule->m_lock);
	i = rule->lock;
	if (i == 1)
	{
		pthread_mutex_unlock(&rule->m_lock);
		return ;
	}
	printf("%dms\t%d %s\n", get_time_ms(rule), philo->id + 1, str);
	pthread_mutex_unlock(&rule->m_lock);
}

void	init_philo(t_rule *rule, int i)
{
	rule->philo[i].id = i;
	rule->philo[i].last_eat = 0;
	rule->philo[i].ptr = (void *)rule;
}

void	init_mutex(t_rule *rule)
{
	int				i;

	i = 0;
	while (i < rule->nb)
	{
		pthread_mutex_init(&rule->m_fork[i], NULL);
		pthread_mutex_init(&rule->m_eating[i], NULL);
		rule->philo[i].m_r_fork = &rule->m_fork[i];
		rule->philo[i].m_meal = &rule->m_eating[i];
		if (i != rule->nb - 1)
			rule->philo[i].m_l_fork = &rule->m_fork[i + 1];
		else
			rule->philo[i].m_l_fork = &rule->m_fork[0];
		i++;
	}
}

int	main(int ac, char **av)
{
	t_rule		rule;
	int			i;
	pthread_t	tid[200];

	if (init_main(ac, av, &rule) == 1)
		return (0);
	init_mutex(&rule);
	i = 0;
	while (i < rule.nb)
	{
		init_philo(&rule, i);
		if (pthread_create(&tid[i], NULL, multi_philos_cycle, &rule.philo[i]))
			return (1);
		i += 2;
		if (i >= rule.nb && i % 2 == 0)
			i = 1;
	}
	while (!rule.lock)
		routine_philo(&rule);
	join_and_destroy_mutex(&rule, tid);
	return (0);
}
