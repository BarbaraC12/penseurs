#ifndef HEADER_H
# define HEADER_H

# include <pthread.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/time.h>
# include <unistd.h>

# define RFORK	"take a fork right 🍴"
# define LFORK	"take a fork left 🍴"
# define EAT	"is eating 🌯"
# define THINK	"is thinking"
# define SLEEP	"is sleeping 😴"
# define DIE	"is dead 👻💀"
# define ATH	"YmNhbm8"
# define USAGE	"Usage: ./philo [nb_philo] [t_die] [t_eat] [t_sleep] ([loop])\n"
# define INT_MAX	2147483647
# define ERR_I	"Invald input:"

typedef struct s_philo {
	int				id;
	void			*ptr;
	int				last_eat;
	pthread_mutex_t	*m_r_fork;
	pthread_mutex_t	*m_l_fork;
	pthread_mutex_t	*m_meal;
	int				cycles;
	int				end;
}	t_philo;

typedef struct s_rule {
	int					nb;
	int					t_die;
	int					t_eat;
	int					t_sleep;
	int					eq;
	int					num_eat;
	int					max_eat;
	long unsigned int	init_usec_ms;
	t_philo				philo[200];
	pthread_mutex_t		m_fork[200];
	pthread_mutex_t		m_eating[200];
	pthread_mutex_t		m_lock;
	int					lock;
	int					error;
}	t_rule;

int		convert_arg(const char *str);
void	write_action(t_philo *philo, t_rule *rule, char *str);
void	quit_convert_arg(char *err, char *txt);
size_t	ft_strlen(const char *s);

int		get_time_ms(t_rule *rule);
void	init_time(t_rule *rule);

int		init_main(int ac, char **av, t_rule *rule);
void	stock_arg(t_rule *rule, int ac, char **av);
void	is_secure(t_rule *a);
int		one_philo_cycle(t_rule *rule);

void	even_one_day_of_life(t_rule *rule, t_philo *philo, int *j);
void	odd_one_day_of_life(t_rule *rule, t_philo *philo, int *j);

void	init_philo(t_rule *rule, int i);
void	standing_action(t_rule *rule, int time_ms);
void	*multi_philos_cycle(void *arg);
void	routine_philo(t_rule *rule);

#endif
