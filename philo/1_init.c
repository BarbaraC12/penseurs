#include "header.h"

void	stock_arg(t_rule *init, int ac, char **av)
{
	init->nb = convert_arg(av[1]);
	init->t_die = convert_arg(av[2]);
	init->t_eat = convert_arg(av[3]);
	init->t_sleep = convert_arg(av[4]);
	if (ac == 6)
	{
		init->num_eat = convert_arg(av[5]);
		init->max_eat = 1;
	}
	else
	{
		init->num_eat = INT_MAX;
		init->max_eat = 0;
	}
}

void	is_secure(t_rule *init)
{
	if (init->nb > 200)
	{
		printf("Danger: so many philosophers\n");
		init->error = 1;
	}
	if (init->nb == 0 || init->t_die < 60
		|| init->t_eat < 60 || init->t_sleep < 60 || init->num_eat <= 0)
	{
		if (init->nb == 0 || init->t_die == 0 || init->t_eat == 0
			|| init->t_sleep == 0 || init->num_eat == 0)
			quit_convert_arg(ERR_I, "value can't be null");
		else if (init->t_die < 60 || init->t_eat < 60 || init->t_sleep < 60)
			quit_convert_arg(ERR_I, "please set values to at least 60ms");
		init->error = 1;
	}
}

int	one_philo_cycle(t_rule *rule)
{
	printf("%dms\t%d %s\n", get_time_ms(rule), 1, RFORK);
	standing_action(rule, rule->t_die);
	printf("%dms\t%d %s\n", get_time_ms(rule), 1, DIE);
	return (1);
}

int	init_main(int ac, char **av, t_rule *init)
{
	init->error = 0;
	if (ac != 5 && ac != 6)
	{
		printf(USAGE);
		init->error = 1;
		return (1);
	}
	stock_arg(init, ac, av);
	is_secure(init);
	if (init->error == 1)
		return (1);
	init_time (init);
	init->lock = 0;
	pthread_mutex_init(&init->m_lock, NULL);
	if (init->nb == 1 && init->num_eat != 0)
		return (one_philo_cycle(init));
	return (0);
}
