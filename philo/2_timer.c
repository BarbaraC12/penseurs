#include "header.h"

void	init_time(t_rule *clock)
{
	struct timeval	t;

	gettimeofday(&t, NULL);
	clock->init_usec_ms = t.tv_sec * 1000 + t.tv_usec / 1000;
}

int	get_time_ms(t_rule *clock)
{
	int				time;
	struct timeval	t;

	gettimeofday(&t, NULL);
	time = t.tv_sec * 1000 + t.tv_usec / 1000 - clock->init_usec_ms;
	return (time);
}
