# Philosophers v9.
I’ve never thought philosophy would be so deadly.

Philosophers is a training project of 42 schools in multi-threading / multi-process and the optimization of the use of the CPU to ensure the survival of all philosophers.

:warning: **Warning**: It is educational project.

:warning: **Warning**: You can take inspiration from it but please don't copy / paste what you don't understand.

## SOMMARY

This project allow us to learn the basics of threading a process, learn how to make threads, and discovery the mutex.
One or more philosophers are sitting at a round table doing one of three things: eating, thinking, or sleeping.

## Don't forget the leaks fd :

On LINUX use valgrind to check if your program LEAKS. Use flag for check leaks fd.

#### Arguments

- Number of arguments: 3 < arg < 7
- There are as many forks as there are philosophers, there will be a fork at the right and the left of each philosopher
- To avoid philosophers duplicating forks, you should protect the forks state with a mutex for each of them.
- Each philosopher should be a thread
- Warning process should return error when negative value is pass

## How run

### On linux

- To compile:
```sh
./philo nb_philo time_die time_eat time_sleep [nb_loop]
```

@bcano 2021


PS : thank you to all my working group for advice and TIPS (especially to lnoailles, pcharton)
