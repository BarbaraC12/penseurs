#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

// A normal C function that is executed as a thread
int		num_thread = 0;
pthread_mutex_t mutex;
#define THR		6
/*
// Mutex will block your program until each thread finish execute cmd
// with mutex mail == 4000000 without mail = random value between 0 & 4000000
pthread_mutex_t mutex;
pthread_mutex_init(&mutex, NULL);
pthread_mutex_lock(&mutex);
pthread_mutex_unlock(&mutex);
pthread_mutex_destroy(&mutex);
*/
/*
//Process
// pid will be the same inter process with thread
// in process (fork()) use two different pid
int myProcess(int t, int i)
{
	int	pid; 
	pid = fork();
	if (pid == -1) 
		wait(NULL);
	if (pid == 0){
		i++;
	}
	//printf("Hello from process %d-%d, id = %d\n", t, i, getpid());
	if (pid != 0){
		wait(NULL);
	}
	//sleep(3);
	return (i);
}
*/
/*
// Thread
int 	x = 1;
int		mail = 0;

void *myThread(void *vargp)
{
	(void)vargp;
	num_thread++;
	for (int c = 0; c < 1000000; c++)
	{
		pthread_mutex_lock(&mutex);
		mail++;
		pthread_mutex_unlock(&mutex);
	}
	//x = myProcess(num_thread, x);
	return NULL;
}

int main(void)
{
	pthread_t	th[THR];
	int			i = 0;
	pthread_mutex_init(&mutex, NULL);
	printf("Begin program\n");
	while (i < THR) // to create thread in loop 
	{
		if (pthread_create(th + i, NULL, myThread, NULL) != 0)
			exit(EXIT_FAILURE);
		printf("Thread %d has started\n", i+1);
		sleep(1);
		i++;
	}
	i = 0;
	while (i < THR) // to create thread in loop 
	{
		if (pthread_join(th[i], NULL) != 0)
			exit(EXIT_FAILURE);
		printf("Thread %d has finished execution\n", i+1);
		sleep(1);
		i++;
	}
	pthread_mutex_destroy(&mutex);
	printf("(%d) threads at END for %d mails\n", num_thread, mail);
	return (0);
}
*/

// Get return value from a thread

/*
void	*roll_dice(){
	int	value  = (rand() % 6)+ 1;
	int* result = malloc(sizeof(int));
	*result =  value;
	printf("Adress result = %p\n", result);
	return ((void*) result);
}

int		main(int argc, char **argv){
	int		*res;
	(void)argv;
	srand(time(NULL));
	pthread_t	th;
	if (argc == 2)
	{
		printf("./a.out : usage ./a.out [numbre]\n");
		exit(EXIT_SUCCESS);
	}
	if (pthread_create(&th, NULL, &roll_dice, NULL) != 0){
		exit(1);
	}
	if (pthread_join(th, (void **) &res) != 0){ // <<<<<======== Here
		exit(2);
	}
	printf("Adress res = %p\n", res); // <<<<<======== Address is same as result
	printf("value = %d\n", *res);
	free(res);
	exit(EXIT_SUCCESS);
}
*/
// Pass arguens to a thread
/*
*/
int		fibo[10] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89};

void*	routine(void* arg){
	sleep(1);
	int	id  = *(int*)arg;
	printf("%d ", fibo[id]);
	free(arg);
	return NULL;
}

int		main(int argc, char* argv[]){
	(void)argv;
	int			i = 0;
	pthread_t	th[10];
	if (argc == 2)
	{
		printf("./a.out : usage ./a.out [numbre]\n");
		exit(EXIT_SUCCESS);
	}
	while (i < 10) 
	{
		int*	a = malloc(sizeof(int));
		*a = i;
		if (pthread_create(&th[i], NULL, &routine, a) != 0)
			exit(1);
		i++;
	}
	i = 0;
	while (i < 10)
	{
		if (pthread_join(th[i], NULL) != 0)
			exit(2);
		i++;
	}
	printf("\n");
	exit(EXIT_SUCCESS);
}
